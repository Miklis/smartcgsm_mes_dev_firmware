#include "protocol.h"
#include "modules.h"
#include "global.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

uint32_t protocol_timestamp;

/*
 * Sends ACK or NACK message
 *
 * parameter:
 * 			message_type - 7 = sends ACK ; 8 = sends NACK
 *
 */
void protocol_send_ACK_message(uint8_t message_type){
	uint8_t message_real_len = PROTOCOL_HEADER_LEN + PROTOCOL_END_LINE_LEN;
	uint8_t message[message_real_len];
	uint16_t message_len = 0;
	uint32_t timestamp = protocol_timestamp + modules_main_loop_interval;

	if(message_type == PROTOCOL_MESSAGE_TYPE_ACK || message_type == PROTOCOL_MESSAGE_TYPE_NACK){
		memcpy(message, &message_type, PROTOCOL_OPCODE_LEN);
		memcpy(message + PROTOCOL_OPCODE_LEN, &message_len, PROTOCOL_MESSAGE_LEN_LEN);
		memcpy(message + PROTOCOL_OPCODE_LEN + PROTOCOL_MES_LEN_PAR, &timestamp, PROTOCOL_TIMESTAMP_LEN);
		memcpy(message + PROTOCOL_HEADER_LEN, "\r\n", PROTOCOL_END_LINE_LEN);

		HAL_UART_Transmit(&huart1, message, PROTOCOL_HEADER_LEN + PROTOCOL_END_LINE_LEN, 1000);
	}
}


/*
 * Check protocol header if opcode and message len meets protocol specification.
 *
 * If opcode of message len is incorrect return -1 and sends NACK messager otherwise returns 0.
 */
uint8_t protocol_check_header(char *message, uint8_t *opcode, uint16_t *mes_len) {
	uint8_t code = message[0];
	uint16_t len = 0;
	uint16_t coutered_mes_len = sensors_message_buffer_index;
	uint32_t time = 0;

	memcpy(&len, message + PROTOCOL_OPCODE_LEN, 2);
	memcpy(&time, message + PROTOCOL_OPCODE_LEN + PROTOCOL_MESSAGE_LEN_LEN, 4);


	if (code >= PROTOCOL_OPCODE_MIN && code <= PROTOCOL_OPCODE_MAX) {
		if (len >= 0 && len <= PROTOCOL_MAX_MESSAGE_LEN) {
			*opcode = code;
			*mes_len = len;
			protocol_timestamp = time;

			if((sensors_message_buffer_index - (PROTOCOL_HEADER_LEN + PROTOCOL_END_LINE_LEN)) == len){
				return 0;
			}
		}
	}

	protocol_send_ACK_message(PROTOCOL_MESSAGE_TYPE_NACK);
	return -1;

}

void protocol_process_module_request_data_message(char *recived_mes,
		uint16_t *mes_len) {
	uint8_t num_req_modules = *mes_len - 1; //-1 request type (Byte)
	uint32_t timestamp = protocol_timestamp + modules_main_loop_interval;
	uint16_t message_body_max_len = (num_req_modules
			* PROTOCOL_MAX_MODULE_DATA_LEN) + PROTOCOL_HEADER_LEN;
	uint16_t message_body_real_len = 0;
	uint8_t message[message_body_max_len];
	uint8_t modules_ids[num_req_modules];
	uint8_t i;
	uint8_t message_type = PROTOCOL_MESSAGE_TYPE_DATA;

	memset(message, 0, message_body_max_len);

	for (i = 0; i < num_req_modules; i++) {
		modules_ids[i] = (uint8_t) recived_mes[PROTOCOL_HEADER_LEN + 1 + i];
	}

	memcpy(message, &message_type, PROTOCOL_OPCODE_LEN);

	modules_get_data(message + PROTOCOL_HEADER_LEN, &message_body_real_len,
			modules_ids, num_req_modules);

	memcpy(message + PROTOCOL_OPCODE_LEN, &message_body_real_len, 2);
	memcpy(message + PROTOCOL_OPCODE_LEN + PROTOCOL_MES_LEN_PAR, &timestamp, 4);
	memcpy(message + PROTOCOL_HEADER_LEN + message_body_real_len, "\r\n", 2);

	HAL_Delay(50);
//	CDC_Transmit_FS(message, strlen(message));


	HAL_UART_Transmit(&huart1, message,
	PROTOCOL_HEADER_LEN + message_body_real_len + 2, 1000);

}

void protocol_process_module_request_info_message() {
	uint32_t timestamp = protocol_timestamp + modules_main_loop_interval;
	uint16_t message_body_max_len = MODULES_NUM * PROTOCOL_MAX_MODULE_INFO_LEN;
	uint16_t message_len = (message_body_max_len) + PROTOCOL_HEADER_LEN
			+ PROTOCOL_REQ_HEADER_LEN;
	uint16_t message_body_real_len = 0;
	uint8_t message[message_len];
	uint8_t message_body[message_body_max_len];
	uint8_t message_type = PROTOCOl_MESSAGE_TYPE_INFO;
	char temp[10];

	memset(message, 0, message_len);
	memset(message_body, 0, message_body_max_len);

	modules_get_module_info(message_body, &message_body_real_len);

	memcpy(message, &message_type, 1);
	memcpy(message + PROTOCOL_OPCODE_LEN, &message_body_real_len, 2);
	memcpy(message + PROTOCOL_OPCODE_LEN + PROTOCOL_MES_LEN_PAR, &timestamp, 4);
	memcpy(message + PROTOCOL_HEADER_LEN, message_body, message_body_real_len);
	memcpy(message + PROTOCOL_HEADER_LEN + message_body_real_len, "\r\n", 2);

	HAL_UART_Transmit(&huart1, message,
	PROTOCOL_HEADER_LEN + message_body_real_len + 2, 1000);

}

void protocol_proccess_module_request_message(char *message, uint16_t *mes_len) {
	uint8_t code = message[7];

	switch (code) {
	case 1:
		if (sensors_message_data_procesing_flag != 0) {
			protocol_process_module_request_data_message(message, mes_len);
		}else{
			protocol_send_ACK_message(PROTOCOL_MESSAGE_TYPE_NACK);
		}
		break;
	case 2:
		protocol_process_module_request_info_message();
		break;
	default:
		protocol_send_ACK_message(PROTOCOL_MESSAGE_TYPE_NACK);
		break;
	}

}


void protocol_procces_module_setting_message(char *message, uint16_t *mes_len){
	uint8_t i,j;
	uint8_t modules_num = *mes_len / PROTOCOL_ONE_MODULE_SETTING_LEN;
	uint8_t id = 0;
	uint8_t sleep = 0;
	uint8_t mes_offset = 0;
	uint16_t read_interval = 0;
	Module_settings_t modules[modules_num];
//	char temp[30];


	for(i = 0; i < modules_num; i++){
//		memset(temp, 0, 30);
		mes_offset = PROTOCOL_HEADER_LEN + 1 + (i * PROTOCOL_ONE_MODULE_SETTING_LEN);

		memcpy(&id, message + mes_offset, sizeof(uint8_t));
		memcpy(&read_interval, message + mes_offset + 1, sizeof(uint16_t));
		memcpy(&sleep, message + mes_offset + 3, sizeof(uint8_t));


		modules[i].module_id 	 = id;
		modules[i].read_interval = read_interval;
		modules[i].sleep		 = sleep;

//		sprintf(temp, "%d - %d - %d \n", id, read_interval, sleep);
//		CDC_Transmit_FS((uint8_t*) temp, strlen(temp));
		HAL_Delay(50);
	}

	if ( modules_set_modules(modules, modules_num) == 0){
		protocol_send_ACK_message(PROTOCOL_MESSAGE_TYPE_ACK);
	}else{
		protocol_send_ACK_message(PROTOCOL_MESSAGE_TYPE_NACK);
	}

}


void protocol_procces_main_loop_setting_message(char *message){
	uint16_t main_loop_interval = 0;
	memcpy(&main_loop_interval, message + PROTOCOL_HEADER_LEN + 1, sizeof(uint16_t));


	if(main_loop_interval >= 50 && main_loop_interval <= 1000){
		modules_main_loop_interval = main_loop_interval;
		HAL_Delay(50); //delay for set global atributte
		protocol_send_ACK_message(PROTOCOL_MESSAGE_TYPE_ACK);
	}else{
		protocol_send_ACK_message(PROTOCOL_MESSAGE_TYPE_NACK);
	}
}

void protocol_procces_setting_message(char *message, uint16_t *mes_len){
	uint8_t code = message[7];

	char temp[15];

	switch(code){
	case 1:
		protocol_procces_module_setting_message(message, mes_len);
		break;
	case 2:
		protocol_procces_main_loop_setting_message(message);
		break;
	default:
		protocol_send_ACK_message(PROTOCOL_MESSAGE_TYPE_NACK);
		break;

	}

}





void protocol_proccess_message(char *message) {
	uint8_t opcode = -1;
	uint16_t mes_len = 0;

//	char temp[100];
//	memset(temp, 0, 100);

//	CDC_Transmit_FS((uint8_t*) message, strlen(message));
	HAL_Delay(50);
	//if (strlen(message) >= HEADER_LEN) {
	if (protocol_check_header(message, &opcode, &mes_len) == 0) {
//		sprintf(temp, "%d - %d - %d - len: %d\n", opcode, mes_len, protocol_timestamp, strlen(message));
//		CDC_Transmit_FS((uint8_t*) temp, strlen(temp));

		switch (opcode) {
		case PROTOCOL_MESSAGE_TYPE_START:
			modules_set_data_processing_flag(true);
			protocol_send_ACK_message(PROTOCOL_MESSAGE_TYPE_ACK);
			break;
		case PROTOCOl_MESSAGE_TYPE_INFO:
			break;
		case PROTOCOl_MESSAGE_TYPE_SETT:
			protocol_procces_setting_message(message, &mes_len);
			break;
		case PROTOCOL_MESSAGE_TYPE_DATA:
			break;
		case PROTOCOL_MESSAGE_TYPE_REQ:
			protocol_proccess_module_request_message(message, &mes_len);
			break;
		case PROTOCOl_MESSAGE_TYPE_STOP:
			modules_set_data_processing_flag(false);
			protocol_send_ACK_message(PROTOCOL_MESSAGE_TYPE_ACK);
			break;
		case PROTOCOL_MESSAGE_TYPE_ACK:
			break;
		case PROTOCOL_MESSAGE_TYPE_NACK:
			break;
		default:
			protocol_send_ACK_message(PROTOCOL_MESSAGE_TYPE_NACK);
			break;
		}
	}
}

