#include "stm32f1xx_hal.h"
#include "neo6m.h"
#include "global.h"
#include "utils.h"
#include "string.h"
#include "modul_data_utils.h"
#include <stdbool.h>
#include <math.h>

/* Indicates if data is in memory from DMA*/
uint8_t data_ready = 0;

/* Buffuer for recieved data from neo6m */
uint8_t gps_data[255];

Module_return_value_t circular_buffer[CIRCULAR_BUFFER_LEN];
uint8_t gps_circular_buffer_index = 0;

void NEO_6M_sleep(bool sleep, uint8_t module_id){

}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	data_ready = 1;
}

uint8_t NEO_6M_calc_nmea_checksum(const uint8_t *message) {
    uint8_t checksum = 0;
    uint8_t i = 0;

    for (i = 1; i < strlen((char *) message) - 4; i++) {
        checksum = checksum ^ (uint8_t) message[i];
    }

    return checksum;
}

uint8_t check_checksum(uint8_t calculated_checksum, uint8_t *checksum) {
    uint8_t checksum_dec = 0;

    checksum_dec += utils_hex_num_to_dec(checksum[1], 1);

    if (checksum_dec < 0)
        return 0;

    checksum_dec += utils_hex_num_to_dec(checksum[2], 0);
    if (checksum_dec < 0)
        return 0;

    if (checksum_dec == calculated_checksum) {
        return 1;
    } else {
        return 0;
    }

}

/*
 * Save recieved coordinates like 4byte signed integer
 *
 */
void NEO_6M_convert_coordinates_to_uint32(int32_t *num ,char *token,  uint8_t wh_num){
		uint8_t wh_part = 5;
		uint8_t	fp_part = 6;
	    int32_t temp = 0;
		char dec[wh_part];
	    char fp[fp_part];

	    memset(dec, 0, wh_part);
	    memcpy(dec, token, wh_num);

	    temp  = atoi(dec);

	    *num = temp * 100000;

	    memset(fp, 0, fp_part);
	    memcpy(fp, token + wh_num + 1, wh_part);

	    temp = atoi(fp);

	    *num += temp;
}


/**
 * This method process GPGLL message and get from message latitude, longtitude and time (UFC+0)
 *
 * Method return values throught pointers:
 * 										 lat - latitude. If latitude is South return negative value (north return positive number)
 * 										 lon - longtitude. If longtitude is West return negative value (East return positive value)
 * 										 time - time in string format hhmmss.
 *
 * Return value:
 * 				0 - ok
 * 				1 - calculated checksum and transport checksum is not same
 *
 */
uint8_t NEO_6M_get_GPGLL_data(uint8_t *gpgll_data, int32_t *lat, int32_t *lon,
                              uint8_t *time) {
    uint8_t i = 0;
    uint8_t gpgll_data_len = strlen((char *)gpgll_data);
    uint8_t checksum = NEO_6M_calc_nmea_checksum(gpgll_data);
    char *data_check = strstr((char *) gpgll_data, "*");
    char *token = strtok((char *) gpgll_data, ",");

    if(gpgll_data_len > 49){
        if (check_checksum((uint8_t) checksum, (uint8_t *) data_check) == 1) {
            while (token != NULL) {
                switch (i) {
                    case 1: /* Latitude processing */
                       NEO_6M_convert_coordinates_to_uint32(lat, token, 4);
                       break;
                    case 2:/* South or North */
                        if (token[0] == 'S') {
                            *lat *= -1;
                        }
                        break;
                    case 3: /* Longtitude processing */
                    	NEO_6M_convert_coordinates_to_uint32(lon, token, 5);
                    	break;
                    case 4: /* West or East */
                        if (token[0] == 'W') {
                            *lon *= -1;
                        }
                        break;
                    case 5: /* Time processing */
                        memset(time, 0, 7);
                        memcpy(time, token, 6);
                        break;
                }

                i++;
                token = strtok(NULL, ","); /* Next parsed part */
            }
        } else {
            return 1;
        }
    }else{
        return 2;
    }

    return 0;
}

uint8_t NEO_6M_read_gps_data(Module_return_value_t *data, uint8_t module_id){
	uint8_t i;
	data->data_num = circular_buffer[gps_circular_buffer_index].data_num;
	char temp[50];

	for(i = 0; i < 3; i++){
		data->data[i].data_len = circular_buffer[gps_circular_buffer_index].data[i].data_len;
		data->data[i].data_type = circular_buffer[gps_circular_buffer_index].data[i].data_type;
		memcpy(data->data[i].data_dsc, circular_buffer[gps_circular_buffer_index].data[i].data_dsc, strlen( circular_buffer[gps_circular_buffer_index].data[i].data_dsc));
		memcpy(data->data[i].data, circular_buffer[gps_circular_buffer_index].data[i].data, data->data[i].data_len);
	}
}

void NEO_6M_save_data_to_buff() {
	int32_t latitude = 0;
	int32_t longtitude = 0;
	uint8_t time[7];
	uint8_t flag = 0;

	char cur_data[255];
	char temp[50];
	char *token;

	if (data_ready == 1) {
		memset(cur_data, 0, 255);
		memcpy(cur_data, gps_data, strlen(gps_data));

		token = strtok((char *)cur_data, "\n");


		while (token != NULL) {
			if (strstr(token, "GPGLL") != NULL) {
				if (NEO_6M_get_GPGLL_data((uint8_t *)token, &latitude, &longtitude, time) == 0) {


					circular_buffer[gps_circular_buffer_index].data_num = 3;
					circular_buffer[gps_circular_buffer_index].data[0].data_type = TYPE_INT32;
					circular_buffer[gps_circular_buffer_index].data[0].data_len  = sizeof(int32_t);
					memcpy(circular_buffer[gps_circular_buffer_index].data[0].data_dsc, "lat\0", 4);
					memcpy(circular_buffer[gps_circular_buffer_index].data[0].data, &latitude, circular_buffer[gps_circular_buffer_index].data[0].data_len);

					circular_buffer[gps_circular_buffer_index].data[1].data_type = TYPE_INT32;
					circular_buffer[gps_circular_buffer_index].data[1].data_len  = sizeof(int32_t);
					memcpy(circular_buffer[gps_circular_buffer_index].data[1].data_dsc, "lon\0", 4);
					memcpy(circular_buffer[gps_circular_buffer_index].data[1].data, &longtitude, circular_buffer[gps_circular_buffer_index].data[1].data_len);

					circular_buffer[gps_circular_buffer_index].data[2].data_type = TYPE_CHAR;
					circular_buffer[gps_circular_buffer_index].data[2].data_len  = 7;
					memcpy(circular_buffer[gps_circular_buffer_index].data[2].data_dsc, "time\0", 5);
					memcpy(circular_buffer[gps_circular_buffer_index].data[2].data, time, circular_buffer[gps_circular_buffer_index].data[2].data_len);

				} else {
					flag = 1;
					circular_buffer[gps_circular_buffer_index].data_num = 1;
					circular_buffer[gps_circular_buffer_index].data[0].data_type = TYPE_UINT8;
					circular_buffer[gps_circular_buffer_index].data[0].data_len  = sizeof(uint8_t);
					memcpy(circular_buffer[gps_circular_buffer_index].data[0].data_dsc, "No loc\0", 7);
					memcpy(circular_buffer[gps_circular_buffer_index].data[0].data, &flag, circular_buffer[gps_circular_buffer_index].data[0].data_len);
				}

				utils_increse_circular_buffer_index(&gps_circular_buffer_index, CIRCULAR_BUFFER_LEN);
			}

			token = strtok(NULL, "\n");
		}

		data_ready = 0;
	}
}

void NEO_6M_process_data() {
	//NEO_6M_save_data_to_buff();
	if(data_ready != 1){
		HAL_UART_Receive_DMA(&huart2, gps_data, 255);
	}

	NEO_6M_save_data_to_buff();
}

/*
 * Inits circular buffer to zero values
 */
void NEO_6M_gps_init() {
	int32_t lat = 0;
	int32_t lon = 0;
	uint8_t time[8];

	memset(time, 0 , sizeof(8));


	circular_buffer[gps_circular_buffer_index].data_num = 3;
	circular_buffer[0].data[0].data_type = TYPE_INT32;
	circular_buffer[0].data[0].data_len  = sizeof(int32_t);
	memcpy(circular_buffer[0].data[0].data_dsc, "lat\0", 4);
	memcpy(circular_buffer[0].data[0].data, &lat, circular_buffer[gps_circular_buffer_index].data[0].data_len);

	circular_buffer[0].data[1].data_type = TYPE_INT32;
	circular_buffer[0].data[1].data_len  = sizeof(int32_t);
	memcpy(circular_buffer[0].data[1].data_dsc, "lon\0", 4);
	memcpy(circular_buffer[0].data[1].data, &lon, circular_buffer[gps_circular_buffer_index].data[1].data_len);

	circular_buffer[0].data[2].data_type = TYPE_CHAR;
	circular_buffer[0].data[2].data_len  = 8;
	memcpy(circular_buffer[0].data[2].data_dsc, "time\0", 5);
	memcpy(circular_buffer[0].data[2].data, time, circular_buffer[gps_circular_buffer_index].data[2].data_len);
}
