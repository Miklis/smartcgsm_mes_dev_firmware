#include "utils.h"


void utils_increse_circular_buffer_index(uint8_t *index, uint8_t buffer_len){
	if(*index == buffer_len - 1){
		*index = 0;
	}else{
		(*index)++;
	}
}


uint8_t utils_max_2th_order_pow(uint8_t num, uint8_t order){
	if(order == 0){
		return 1;
	}else if(order == 1){
		return num;
	}else if(order == 2){
		return num * num;
	}else {
		return 0;
	}
}

/**
 * Convert hex number to decimal number.
 *
 * Parameters:
 * 			  hex_num - hexadecimal number in char format
 * 			  order   - numeric order of hexadecimal number
 *
 * Return value: Return decimal number from 0 to 9 or return -1 if
 * hex number isnt correct.
 */
int8_t utils_hex_num_to_dec(uint8_t hex_num, uint8_t order){
    int8_t result = 0;

    if((hex_num >= (uint8_t)('A') && hex_num <= (uint8_t)('F'))){
        result = ((hex_num - (uint8_t)('A')) + 10) * utils_max_2th_order_pow(16, order);
    }else if((hex_num >= (uint8_t)('a') && hex_num <= (uint8_t)('f'))){
        result = ((hex_num - (uint8_t)('a')) + 10) * utils_max_2th_order_pow(16, order);
    }else if((hex_num >= (uint8_t)('0') && hex_num <= (uint8_t)('9'))){
        result = (hex_num - (uint8_t)('0')) * utils_max_2th_order_pow(16, order);
    }else {
        result = -1;
    }

    return  result;
}

