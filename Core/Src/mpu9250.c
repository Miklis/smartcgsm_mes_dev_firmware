#include <modul_data_utils.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "mpu9250.h"
#include "global.h"
#include "modules.h"
#include "utils.h"

/* MPU_9250_I2C addres */
#define MPU_9250_ADDR 0x68<<1

/* Who ami register */
#define MPU_9250_WHO_AM_I_REQ 0x75

/* Power management register 1 */
#define MPU_9250_PWR_MGMZ_1	  0x6B

/* Power management register 2 */
#define MPU_9250_PWR_MGMZ_2	  0x6C

/* Sensor data processing frequency */
#define MPU_9250_SMPLRT_DIV   0x19

/* Accelometr precision register */
#define MPU_9250_ACCEL_CONFIG 0x1C

/* Accelometr start data register */
#define MPU_9250_ACCEL_XOUT_H 0x3B

/* Gyroscope precision register*/
#define MPU_9250_GYRO_CONFIG  0x1B

/* Gyroscope start data register */
#define MPU_9250_GYRO_XOUT_H  0x43

/* 1G */
#define MPU_9250_GRAVITY 1000


#define ACC_SCALING_FACTOR_2G 0.061
#define GYRO_SCALING_FACTOR_250 0.0076

/* Number of values in movement table */
#define MOVEMENT_TABLE_NUM 5

#define MOVEMENT_INTESITY_1 50
#define MOVEMENT_INTESITY_2 500
#define MOVEMENT_INTESITY_3 1000
#define MOVEMENT_INTESITY_4 2000


Module_return_value_t circular_buffer_accel[CIRCULAR_BUFFER_LEN];
Module_return_value_t circular_buffer_gyro[CIRCULAR_BUFFER_LEN];
uint8_t wake_up_flag = 0;
uint8_t accel_circular_buffer_index = 0;
uint8_t gyro_circular_buffer_index = 0;


int16_t movement_table[MOVEMENT_TABLE_NUM];

int32_t delta_gravity_samples = 0;
/* init 1G */
int32_t last_accel = 1000;
int32_t current_accel = 0;
int32_t avarge_sum = 0;


/*
 * Check if MPU_9250 is connected.
 *
 * Sets accelerometr range.
 *
 * Wake up MPU_9250 sensors if sleep and sets sensors computing frequency
 *
 */
void MPU_9250_accel_init(){
	uint8_t check = 0, data;

		HAL_Delay(50);
		HAL_I2C_Mem_Read(&hi2c1, MPU_9250_ADDR, MPU_9250_WHO_AM_I_REQ, 1, &check, 1, 1000);
		if(check == 113){


			if(wake_up_flag == 0){
				 data = 0x00;
				 HAL_I2C_Mem_Write(&hi2c1, MPU_9250_ADDR, MPU_9250_PWR_MGMZ_1, 1,&data, 1, 1000);
				 wake_up_flag = 1;
				 //set data rate
				 HAL_Delay(500);
				 data = 0x07;
				 //sets accel range 2G
				 HAL_I2C_Mem_Write(&hi2c1, MPU_9250_ADDR, MPU_9250_SMPLRT_DIV, 1,&data, 1, 1000);
				 HAL_Delay(40);

			}

			data = 0x00;
			HAL_I2C_Mem_Write(&hi2c1, MPU_9250_ADDR, MPU_9250_ACCEL_CONFIG, 1,&data, 1, 1000);
		}
}


/*
 * Check if MPU_9250 is connected.
 *
 * Sets gyroscope range.
 *
 * Wake up MPU_9250 sensors if sleep and sets sensors computing frequency
 *
 */
void MPU_9250_gyro_init(){

	uint8_t check, data;
	HAL_I2C_Mem_Read(&hi2c1, MPU_9250_ADDR, MPU_9250_WHO_AM_I_REQ, 1, &check, 1, 1000);

	if(check == 113){

		if(wake_up_flag == 0){
			 data = 0x00;
			 HAL_I2C_Mem_Write(&hi2c1, MPU_9250_ADDR, MPU_9250_PWR_MGMZ_1, 1,&data, 1, 1000);
			 wake_up_flag = 1;
			 //set data rate
			 HAL_Delay(500);
			 data = 0x07;
			 HAL_I2C_Mem_Write(&hi2c1, MPU_9250_ADDR, MPU_9250_SMPLRT_DIV, 1,&data, 1, 1000);
			 HAL_Delay(40);
		}



		 //set gyro senstitivity 250+-
		 data = 0x00;
		 HAL_I2C_Mem_Write(&hi2c1, MPU_9250_ADDR, MPU_9250_GYRO_CONFIG, 1,&data, 1, 1000);
	}
}


void MPU_9250_sleep_accel(bool sleep, uint8_t module_id){
	uint8_t check, data;

	HAL_I2C_Mem_Read(&hi2c1, MPU_9250_ADDR, MPU_9250_WHO_AM_I_REQ, 1, &check, 1, 1000);
	if(check == 113){
		HAL_I2C_Mem_Read(&hi2c1, MPU_9250_ADDR, MPU_9250_PWR_MGMZ_2, 1,&data, 1, 1000);
		if(sleep == true){
			data = data | 0x38;
		}else{
			data = data & 0xC7;
		}

		HAL_I2C_Mem_Write(&hi2c1, MPU_9250_ADDR, MPU_9250_PWR_MGMZ_2, 1,&data, 1, 1000);
		HAL_Delay(20);
	}
}

void MPU_9250_sleep_gyro(bool sleep, uint8_t module_id){
	uint8_t check, data;

	HAL_I2C_Mem_Read(&hi2c1, MPU_9250_ADDR, MPU_9250_WHO_AM_I_REQ, 1, &check, 1, 1000);
	if(check == 113){
		HAL_I2C_Mem_Read(&hi2c1, MPU_9250_ADDR, MPU_9250_PWR_MGMZ_2, 1,&data, 1, 1000);
		if(sleep == true){
			data = data | 0x07;
		}else{
			data = data & 0xF8;
		}
		HAL_I2C_Mem_Write(&hi2c1, MPU_9250_ADDR, MPU_9250_PWR_MGMZ_2, 1,&data, 1, 1000);
		HAL_Delay(30);
	}
}

void MPU_9250_assing_val_to_mov_table(int16_t av){

	if(av < MOVEMENT_INTESITY_1){
		movement_table[0]++;
	}else if(av < MOVEMENT_INTESITY_2){
		movement_table[1]++;
	}else if(av < MOVEMENT_INTESITY_3){
		movement_table[2]++;
	}else if(av < MOVEMENT_INTESITY_4){
		movement_table[3]++;
	}else{
		movement_table[4]++;
	}
}

void MPU_9250_calculate_movement(int16_t *accel){
	int16_t av;
	int32_t delta;

	last_accel = current_accel;
	current_accel = ((accel[0] * accel[0]) + (accel[1] * accel[1]) + (accel[2] * accel[2])) / MPU_9250_GRAVITY;
	delta = current_accel - last_accel;
	delta_gravity_samples++;

	avarge_sum += abs(delta);
	av = avarge_sum / delta_gravity_samples;

	if(delta_gravity_samples == 10){
		delta_gravity_samples = 0;
		avarge_sum = 0;
		MPU_9250_assing_val_to_mov_table(av);
	}

}

void MPU_9250_procces_accel(){
	uint8_t read_data[6];
	uint8_t check;
	uint8_t i = 0;
	int16_t accel [3];

	MPU_9250_sleep_accel(false, 0);


	circular_buffer_accel[accel_circular_buffer_index].data_num = 3;

	circular_buffer_accel[accel_circular_buffer_index].data[0].data_len = sizeof(int16_t);;
	circular_buffer_accel[accel_circular_buffer_index].data[0].data_type = TYPE_INT16;
	memcpy(circular_buffer_accel[accel_circular_buffer_index].data[0].data_dsc, "x\0", 2);

	circular_buffer_accel[accel_circular_buffer_index].data[1].data_len = sizeof(int16_t);;
	circular_buffer_accel[accel_circular_buffer_index].data[1].data_type = TYPE_INT16;
	memcpy(circular_buffer_accel[accel_circular_buffer_index].data[1].data_dsc, "y\0", 2);

	circular_buffer_accel[accel_circular_buffer_index].data[2].data_len = sizeof(int16_t);;
	circular_buffer_accel[accel_circular_buffer_index].data[2].data_type = TYPE_INT16;
	memcpy(circular_buffer_accel[accel_circular_buffer_index].data[2].data_dsc, "z\0", 2);


	HAL_I2C_Mem_Read(&hi2c1, MPU_9250_ADDR, MPU_9250_WHO_AM_I_REQ, 1, &check, 1, 1000);


	if(check == 113){
		HAL_I2C_Mem_Read(&hi2c1, MPU_9250_ADDR, MPU_9250_ACCEL_XOUT_H, 1, read_data, 6, 1000);
		HAL_I2C_Mem_Read(&hi2c1, MPU_9250_ADDR, MPU_9250_ACCEL_CONFIG, 1, &check, 1, 1000);


			for(i = 0; i < 3; i++){
				accel[i] = (int16_t)(read_data[i*2] << 8 | read_data[(i*2)+1]);
				accel[i] = accel[i] * ACC_SCALING_FACTOR_2G;
				memcpy(circular_buffer_accel[accel_circular_buffer_index].data[i].data, &accel[i], circular_buffer_accel[accel_circular_buffer_index].data[i].data_len);
			}

			MPU_9250_calculate_movement(accel);
	}
	utils_increse_circular_buffer_index(&accel_circular_buffer_index, CIRCULAR_BUFFER_LEN);
	MPU_9250_sleep_accel(true, 0);
}

uint8_t MPU_9250_read_accel_data(Module_return_value_t *data, uint8_t module_id){
	uint8_t i;

	data->data_num = 3;
	data->data_num = circular_buffer_accel[accel_circular_buffer_index].data_num;

	for(i = 0; i < data->data_num ; i++){
		data->data[i].data_len = circular_buffer_accel[accel_circular_buffer_index].data[i].data_len;
		data->data[i].data_type = circular_buffer_accel[accel_circular_buffer_index].data[i].data_type;
		memcpy(data->data[i].data_dsc, circular_buffer_accel[accel_circular_buffer_index].data[i].data_dsc, 2);
		memcpy(data->data[i].data, circular_buffer_accel[accel_circular_buffer_index].data[i].data, data->data[i].data_len);
	}

	return 0;
}



void MPU_9250_proccess_gyro(){
	uint8_t read_data[6];
	uint8_t check;
	uint8_t i;
	int16_t gyro[3];

	MPU_9250_sleep_gyro(false, 0);
	circular_buffer_gyro[gyro_circular_buffer_index].data_num = 3;

	circular_buffer_gyro[gyro_circular_buffer_index].data[0].data_len = sizeof(int16_t);;
	circular_buffer_gyro[gyro_circular_buffer_index].data[0].data_type = TYPE_INT16;
	memcpy(circular_buffer_gyro[gyro_circular_buffer_index].data[0].data_dsc, "x\0", 2);

	circular_buffer_gyro[gyro_circular_buffer_index].data[1].data_len = sizeof(int16_t);;
	circular_buffer_gyro[gyro_circular_buffer_index].data[1].data_type = TYPE_INT16;
	memcpy(circular_buffer_gyro[gyro_circular_buffer_index].data[1].data_dsc, "y\0", 2);

	circular_buffer_gyro[gyro_circular_buffer_index].data[2].data_len = sizeof(int16_t);;
	circular_buffer_gyro[gyro_circular_buffer_index].data[2].data_type = TYPE_INT16;
	memcpy(circular_buffer_gyro[gyro_circular_buffer_index].data[2].data_dsc, "z\0", 2);

	HAL_I2C_Mem_Read(&hi2c1, MPU_9250_ADDR, MPU_9250_WHO_AM_I_REQ, 1, &check, 1, 1000);

	if(check == 113){
		HAL_I2C_Mem_Read(&hi2c1, MPU_9250_ADDR, MPU_9250_GYRO_XOUT_H, 1, read_data, 6, 1000);



		for(i = 0; i < 3; i++){
			gyro[i] = (int16_t)(read_data[i*2] << 8 | read_data[(i*2)+1]);
			gyro[i] = (gyro[i] * GYRO_SCALING_FACTOR_250);
			memcpy(circular_buffer_gyro[gyro_circular_buffer_index].data[i].data, &gyro[i], circular_buffer_gyro[gyro_circular_buffer_index].data[i].data_len);
		}
	}

	utils_increse_circular_buffer_index(&gyro_circular_buffer_index, CIRCULAR_BUFFER_LEN);
	MPU_9250_sleep_gyro(true, 0);
}

uint8_t MPU_9250_read_gyro_data(Module_return_value_t *data, uint8_t module_id){
	uint8_t i;
	data->data_num = 3;

	for(i = 0; i < 3; i++){
		data->data[i].data_len = circular_buffer_gyro[gyro_circular_buffer_index].data[i].data_len;
		data->data[i].data_type = circular_buffer_gyro[gyro_circular_buffer_index].data[i].data_type;
		memcpy(data->data[i].data_dsc, circular_buffer_gyro[gyro_circular_buffer_index].data[i].data_dsc, 2);
		memcpy(data->data[i].data, circular_buffer_gyro[gyro_circular_buffer_index].data[i].data, data->data[i].data_len);
	}

	return 0;
}


uint8_t MPU_9250_read_movement_value(Module_return_value_t *data, uint8_t module_id){
	uint8_t i;
	char temp;

	data->data_num = 5;

	for(i = 0; i < MOVEMENT_TABLE_NUM; i++){
		data->data[i].data_len = sizeof(int16_t);
		data->data[i].data_type = TYPE_INT16;
		temp = i + 48;
		memcpy(data->data[i].data_dsc, &temp , 1);
		memcpy(data->data[i].data_dsc + 1, "\0", 1);
		memcpy(data->data[i].data, &movement_table[i], data->data[i].data_len);
	}

	for(i = 0; i < MOVEMENT_TABLE_NUM;i++){
		if(movement_table[i] != 0){
			memset(movement_table, 0, sizeof(movement_table));
			break;
		}
	}

	return 0;
}
