#include "modules.h"
#include "mpu9250.h"
#include "tmp36.h"
#include "neo6m.h"
#include "modul_data_utils.h"
#include "stdio.h"
#include "string.h"
#include "global.h"
#include "protocol.h"
#include <stdbool.h>

/*
   - name
   - dev_type
   - module_id
   - current_read_interval
   - min_read_interval
   - max_read_interval
   - sleep
   - init_func
   - read_func
   - write_func
   - process_data_func
   - sleep_func
   */
Module_t modules[MODULES_NUM] =
	  { {"accel\0", DEV_ACCELOMETR, 1, 1, 	1, 10, 	 0, &MPU_9250_accel_init, &MPU_9250_read_accel_data, 	 NULL, NULL, &MPU_9250_procces_accel, &MPU_9250_sleep_accel},
		{"gyro\0", DEV_GYROSCOPE,   2, 1, 	1, 10, 	 0, &MPU_9250_gyro_init,  &MPU_9250_read_gyro_data,  	 NULL, NULL, &MPU_9250_proccess_gyro, &MPU_9250_sleep_gyro },
		{"temp1\0", DEV_TEMP, 	  	3, 10, 10, 3600, 0, NULL, 				  &TMP_36_get_temperature, 	 	 NULL, NULL, &TMP_36_proccess, 		&TMP_36_sleep},
		{"temp2\0", DEV_TEMP, 		4, 10, 10, 3600, 0, NULL, 				  &TMP_36_get_temperature, 	 	 NULL, NULL, &TMP_36_proccess, 		&TMP_36_sleep},
		{"gps\0", DEV_GPS, 			5, 1, 	1, 3600, 0, &NEO_6M_gps_init, 	  &NEO_6M_read_gps_data, 	 	 NULL, NULL, &NEO_6M_process_data, 	&NEO_6M_sleep },
		{"mov sens\0", DEV_MOVE, 	6, 10, 10, 3600, 0, NULL, 				  &MPU_9250_read_movement_value, NULL, NULL, NULL, 					&MPU_9250_sleep_accel}};

/* Tempororary buffer for recieving message */
char message_buffer[255];

/*Indicates if uart stil reading data from BLE charakteristic */
uint8_t modules_message_reading_flag = 0;

/* Indicates if message reading is complete */
uint8_t modules_message_readed_flag = 0;

/* If variable set to 0 data is not gathering from sensors.
 * If set 1-255 modules gathering data from sensors  */
uint8_t sensors_message_data_procesing_flag = 0;

/* Recieving message leneght */
uint16_t modules_message_len_par = 0;

/* Recieved data index in message_buffer */
uint16_t sensors_message_buffer_index = 0;

/* Time when MCU sleep durign processing main loop */
uint16_t modules_main_loop_interval = 100;

/* Number of processed main loops */
uint16_t loop_counter = 0;

/* Count time when MCU start reading data from BLE UART */
uint16_t message_time_delimiter = 0;


void modules_init_all_modules() {
	int i;
	for (i = 0; i < MODULES_NUM; i++) {
		if (modules[i].init != NULL) {
			modules[i].init();
		}
	}
}

/*
 * Start or stop data gathering procedure
 */
void modules_set_data_processing_flag(bool start){

	if(start == true){
		sensors_message_data_procesing_flag = 1;
	}else{
		sensors_message_data_procesing_flag = 0;
	}

}


/*
 * Apply modules setting from recieved ModuleSettingMessage.
 *
 * If gets from message some invalid data return -1 otherwise 0;
 *
 */
int modules_set_modules(Module_settings_t *module_setting, uint8_t module_num){
	uint8_t i, j;

	for(i = 0; i < module_num; i++){
		for(j = 0; j < MODULES_NUM; j++){

			if(module_setting[i].module_id == modules[j].module_id){
				modules[j].sleep = module_setting[i].sleep;

				if(module_setting[i].read_interval >= modules[j].min_read_interval && module_setting[i].read_interval <= modules[j].max_read_interval){
					modules[j].current_read_interval = module_setting[i].read_interval;
				}else{
					return -1;
				}


				if(modules[j].sleep_func != NULL && module_setting[i].sleep != 0){
					modules[j].sleep_func(true, modules[j].module_id);
				}else{
					modules[j].sleep_func(false, modules[j].module_id);
				}

				continue;
			}
		}
	}




	return 0;
}


/*
 * Returns array of Module_t according to recieved ids.
 *
 */
void sensors_get_requested_modules(Module_t *modules_arr, uint8_t *modules_ids,
		uint8_t modules_num) {
	uint8_t i, j;

	for (i = 0; i < modules_num; i++) {
		for (j = 0; j < MODULES_NUM; j++) {
			;

			if (modules_ids[i] == modules[j].module_id) {
				modules_arr[i] = modules[j];
			}
		}
	}
}



/*
 * Return char array of modules information (id, dev_type, cur_read_interval, min_read_interval, max_read_interval, sleel, and description)
 */
void modules_get_module_info(char *temp, uint16_t *temp_len) {
	uint8_t i;
	uint8_t info_len = 0;
	uint16_t offset = 0;
//	char tempo[50];
//	memset(tempo, 0, 50);

	for (i = 0; i < MODULES_NUM; i++) {
		info_len = strlen(modules[i].name);
		memcpy(temp + offset, &modules[i].module_id,
				sizeof(modules[i].module_id));
		++offset;
		memcpy(temp + offset, &modules[i].dev_type,
				sizeof(modules[i].dev_type));
		++offset;
		memcpy(temp + offset, &modules[i].current_read_interval,
				sizeof(modules[i].current_read_interval));
		offset += sizeof(uint16_t);
		memcpy(temp + offset, &modules[i].min_read_interval,
				sizeof(modules[i].min_read_interval));
		offset += sizeof(uint16_t);
		memcpy(temp + offset, &modules[i].max_read_interval,
				sizeof(modules[i].max_read_interval));
		offset += sizeof(uint16_t);
		memcpy(temp + offset, &modules[i].sleep, sizeof(modules[i].sleep));
		++offset;
		memcpy(temp + offset, &info_len, 1);
		++offset;
		memcpy(temp + offset, modules[i].name, strlen(modules[i].name));
		offset += strlen(modules[i].name);
//		sprintf(tempo, "%s, read: %d, sleep:%d \n",modules[i].name, modules[i].current_read_interval, modules[i].sleep);
//		CDC_Transmit_FS((uint8_t*) tempo, strlen(tempo));
		HAL_Delay(50);
	}

	*temp_len = offset;
}


/*
 * Gets data in char array form from requsted modules
 */
void modules_get_data(uint8_t *temp, uint16_t *message_len,
		uint8_t *modules_ids, uint8_t modules_num) {
	uint8_t i, j;
	uint8_t memory_len = 0;
	uint8_t id = 0;
	uint16_t offset = 0;
	Module_t modules_arr[modules_num];
	Module_return_value_t val = { 0 };
//	char buff[200];


	sensors_get_requested_modules(modules_arr, modules_ids, modules_num);



	for (i = 0; i < modules_num; i++) {
//		memset(buff, 0, 200);
		//CDC_Transmit_FS((uint8_t*) modules_arr[i].name,
		//		strlen(modules_arr[i].name));
		//HAL_Delay(50);

		if (modules_arr[i].read != NULL) {
			//memset(buff, 0, 200);
			memset(&val, 0, sizeof(Module_return_value_t));
			//reading data from module
			modules_arr[i].read(&val, modules_arr[i].module_id);


			//module id
			id = modules_arr[i].module_id;
			memcpy(temp + offset, &id, sizeof(uint8_t));
			++offset;



			//number of module gathered data
			memcpy(temp + offset, &val.data_num, sizeof(val.data_num));
			++offset;

			for (j = 0; j < val.data_num; j++) {
				//data type
				memcpy(temp + offset, &val.data[j].data_type,
						sizeof(val.data[j].data_type));
				++offset;
				//data
				memory_len = val.data[j].data_len;
				memcpy(temp + offset, val.data[j].data, memory_len);
				offset += memory_len;

				//description lenght
				memory_len = strlen(val.data[j].data_dsc);
				memcpy(temp + offset, &memory_len, sizeof(memory_len));
				++offset;

				//description
				memcpy(temp + offset, val.data[j].data_dsc, memory_len);
				offset += memory_len;
			}

//			strncpy(buff, modules_arr[i].name, strlen(modules_arr[i].name));
//			strncpy(buff + strlen(buff), "\n", 1);

//			build_data_info(&val, buff);
//			CDC_Transmit_FS((uint8_t*) buff, strlen(buff));
//			HAL_Delay(50);

			//HAL_UART_Transmit(&huart1, buff, strlen(buff), 2000);

		}
	}

	*message_len = offset;
}


void modules_reset_message_reciving_params(){
		modules_message_readed_flag = 1;
		modules_message_reading_flag = 0;
		modules_message_len_par = 0;
		message_time_delimiter = 0;
}

/*
	Check delimiter expiration. If delimiter has expire reset parametr for recieving data.

 */
void modules_message_reader_delimiter(){
	if(modules_message_reading_flag == 1){
		message_time_delimiter += modules_main_loop_interval;
		if(message_time_delimiter >= BLUETOOTH_RECIEVE_TIME_LIMIT){
			sensors_message_buffer_index = 0;
			modules_reset_message_reciving_params();
		}
	}
}

/*
 * Executes proccess fuction on modules.
 *
 * Processing recived message from BLE.
 *
 */
void modules_main_loop() {
	uint8_t i;
	Module_return_value_t val = {0};
	MPU_9250_read_accel_data(&val, 0);
//	char temp[8];

	if (sensors_message_data_procesing_flag != 0) {
		for (i = 0; i < MODULES_NUM; i++) {

			if (modules[i].proccess_data != NULL) {
				if(modules[i].sleep == 0 && (loop_counter % modules[i].current_read_interval) == 0){
					modules[i].proccess_data();
				}
			}
		}
	}




	//sprintf(temp, "%d \n", sensors_message_data_procesing_flag);
	//CDC_Transmit_FS(temp, strlen(temp));

	if (modules_message_readed_flag == 1) {
		HAL_Delay(100);
		protocol_proccess_message(message_buffer);
		modules_message_readed_flag = 0;
		sensors_message_buffer_index = 0;
		memset(message_buffer, 0, 255);
	}

	loop_counter++;

	modules_message_reader_delimiter();
	HAL_Delay(modules_main_loop_interval);
}

