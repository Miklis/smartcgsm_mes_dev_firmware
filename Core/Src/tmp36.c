#include "tmp36.h"
#include "global.h"
#include "modul_data_utils.h"
#include "utils.h"
#include <string.h>
#include <stdio.h>

#define TMP_36_NUM 				2
#define TMP_36_ADC_BUFF_NUM  	100
#define TMP_36_ADC_BUFF_MIDDLE	49
#define TMP_36_ADC_BUFF_LOW 	0
#define TMP_36_ADC_BUFF_HIGH 	99

uint32_t temp_adc[TMP_36_NUM][TMP_36_ADC_BUFF_NUM], buffer[TMP_36_NUM];
uint8_t processed_adc_vals = 0;
uint8_t tmp_36_data_get = TMP_36_NUM;
uint8_t ready_flag = 1;

Module_return_value_t circular_buffer_tmp1[CIRCULAR_BUFFER_LEN];
Module_return_value_t circular_buffer_tmp2[CIRCULAR_BUFFER_LEN];
uint8_t tmp1_buffer_index = 0;
uint8_t tmp2_buffer_index = 0;

void TMP_36_sleep(bool sleep, uint8_t module_id){

}


/*
 * Calculate avarge adc value
 */
uint32_t cacl_av_adc_val(uint8_t temp_arr_index) {
	uint8_t i;
	uint32_t sum = 0;

	for (i = 0; i < TMP_36_ADC_BUFF_NUM; i++) {

		sum += temp_adc[temp_arr_index][i];
	}

	sum /= TMP_36_ADC_BUFF_NUM;

	return sum;
}

void TMP_36_process_temperatur_value(Module_return_value_t *data, uint8_t adc_arr_pos){
	float res_val;
	uint32_t gathered_val = 0;
	int16_t res_val_fix_p = 0;

	data->data_num = 1;
	memcpy(data->data[0].data_dsc, "cels\0", 5);
	data->data[0].data_type = TYPE_INT16;
	data->data[0].data_len = sizeof(int16_t);


	gathered_val = cacl_av_adc_val(adc_arr_pos);

	res_val = ((float) (gathered_val) / 4096) * 3.3;
	res_val = (res_val - 0.5) * 100;
	res_val_fix_p = res_val * 10;

	memcpy(data->data[0].data, &res_val_fix_p, data->data[0].data_len);
}


/*
 Read adc value from TMP36 and convert it to temperature (°C).
 */
void TMP_36_save_val_to_buff() {
	uint8_t adc_arr_pos;

	adc_arr_pos = 0;
	TMP_36_process_temperatur_value(&circular_buffer_tmp1[tmp1_buffer_index], adc_arr_pos);
	tmp_36_data_get++;

	adc_arr_pos = 1;
	TMP_36_process_temperatur_value(&circular_buffer_tmp2[tmp2_buffer_index], adc_arr_pos);
	tmp_36_data_get++;

}

/*
 * Fuction is executed when data is recieved from DMA controller
 *
 * Start data process. Calc avarge ADC value for both ADC channel a from adc values
 * calculate Temperature
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc) {
	uint8_t i;

	for (i = 0; i < TMP_36_NUM; i++) {
			temp_adc[i][processed_adc_vals] = buffer[i];
	}

	processed_adc_vals++;

	if (processed_adc_vals == TMP_36_ADC_BUFF_NUM) {
		HAL_ADC_Stop_DMA(&hadc1);
		processed_adc_vals = 0;
		ready_flag = 1;
		TMP_36_save_val_to_buff();
	}

}

void TMP_36_proccess() {
	if (tmp_36_data_get == TMP_36_NUM && ready_flag == 1) {
		tmp_36_data_get = 0;
		ready_flag = 0;
		memset(temp_adc, 0, sizeof(temp_adc[0][0]) * TMP_36_NUM * TMP_36_ADC_BUFF_NUM);
		HAL_ADC_Start_DMA(&hadc1, buffer, TMP_36_NUM);
	}

}

void TMP_36_get_data_from_buffer(Module_return_value_t *data, Module_return_value_t *buffer){
	data->data[0].data_len = buffer->data[0].data_len;
	data->data[0].data_type = buffer->data[0].data_type;
	memcpy(data->data[0].data_dsc, buffer->data[0].data_dsc, strlen(buffer->data[0].data_dsc));
	memcpy(data->data[0].data, buffer->data[0].data, data->data[0].data_len);
}

uint8_t TMP_36_get_temperature(Module_return_value_t *data, uint8_t module_id) {
	data->data_num = 1;

	switch (module_id) {
		case 3:
			TMP_36_get_data_from_buffer(data, &circular_buffer_tmp1[tmp1_buffer_index]);
			break;
		case 4:
			TMP_36_get_data_from_buffer(data, &circular_buffer_tmp2[tmp2_buffer_index]);
			break;
	}

	return 0;
}
