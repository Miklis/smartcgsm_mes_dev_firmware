#ifndef __DATACOLECTOR_MPU_9250_H
#define __DATACOLECTOR_MPU_9250_H
#include "stm32f1xx_hal.h"
#include "modules.h"

void MPU_9250_accel_init();
uint8_t MPU_9250_read_accel_data(Module_return_value_t *data, uint8_t module_id);
void MPU_9250_procces_accel();
void MPU_9250_sleep_accel(bool sleep, uint8_t module_id);

void MPU_9250_gyro_init();
uint8_t MPU_9250_read_gyro_data(Module_return_value_t *data, uint8_t module_id);
void MPU_9250_proccess_gyro();
void MPU_9250_sleep_gyro(bool sleep, uint8_t module_id);

uint8_t MPU_9250_read_movement_value(Module_return_value_t *data, uint8_t module_id);


#endif
