#ifndef __MODUL_DATA_UTILS_H
#define __MODUL_DATA_UTILS_H

#include "modules.h"

void build_data_info(Module_return_value_t *data, char *buff);

#endif
