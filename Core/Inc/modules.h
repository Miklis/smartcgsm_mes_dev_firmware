#ifndef MODULES_H
#define MODULES_H
#include "stm32f1xx_hal.h"
#include <stdbool.h>

#define MODULES_NUM 6
#define BLUETOOTH_RECIEVE_TIME_LIMIT 1000


#define MAX_DESC_SIZE 10
#define MAX_DESC_ITEMS 3
#define MAX_DATA_NUM_FROM_MODULE 5

typedef enum data_type{
	TYPE_UINT8  = 1,
	TYPE_UINT16 = 2,
	TYPE_UINT32 = 3,
	TYPE_UINT64 = 4,
	TYPE_INT8   = 5,
	TYPE_INT16 	= 6,
	TYPE_INT32 	= 7,
	TYPE_INT64 	= 8,
	TYPE_FLOAT 	= 9,
	TYPE_DOUBLE = 10,
	TYPE_CHAR 	= 11
}Data_type_t;

typedef enum dev_type{
	DEV_ACCELOMETR = 1,
	DEV_GYROSCOPE  = 2,
	DEV_TEMP	   = 3,
	DEV_HEART_RATE = 4,
	DEV_GPS		   = 5,
	DEV_MOVE	   = 6
}Dev_type_T;

typedef struct the_data{
	Data_type_t data_type;
	char data_dsc[MAX_DESC_SIZE];
	uint8_t data_len;
	uint8_t data[8];
}Data_t;


typedef struct the_module_return_value{
	Data_t data[MAX_DATA_NUM_FROM_MODULE];
	uint8_t data_num;
}Module_return_value_t;


typedef struct the_module{
	char name[32];
	Dev_type_T dev_type;
	uint8_t module_id;
	uint16_t current_read_interval;
	uint16_t min_read_interval;
	uint16_t max_read_interval;
	uint8_t sleep; //0 false or true
	void (*init)();
	uint8_t (*read)(Module_return_value_t *data, uint8_t module_id);
	uint8_t (*read_all)(Module_return_value_t *data, uint8_t module_id);
	void (*write)(uint8_t *data);
	void (*proccess_data)();
	void (*sleep_func)(bool sleep, uint8_t module_id);
}Module_t;

typedef struct the_modules_settings{
	uint8_t module_id;
	uint8_t sleep;
	uint16_t read_interval;
}Module_settings_t;


extern char message_buffer[255];
extern uint8_t modules_message_reading_flag;
extern uint8_t modules_message_readed_flag;
extern uint8_t sensors_message_data_procesing_flag;
extern uint16_t modules_message_len_par;
extern uint16_t sensors_message_buffer_index;
extern uint16_t modules_main_loop_interval;

int  modules_set_modules(Module_settings_t *module_setting, uint8_t module_num);
void modules_set_data_processing_flag(bool start);
void modules_get_data(uint8_t *temp, uint16_t *message_len, uint8_t *modules_ids, uint8_t modules_num);
void modules_get_module_info();
void modules_main_loop();
void modules_print_all_modules_values(I2C_HandleTypeDef *hi2c1, ADC_HandleTypeDef *hadc1);
void modules_init_all_modules();


#endif
