#ifndef __NEO6M_H
#define __NEO6M_H
#include <modules.h>
#include "stm32f1xx_hal.h"


uint8_t NEO_6M_read_gps_data(Module_return_value_t *data, uint8_t module_id);
void NEO_6M_process_data();
void NEO_6M_gps_init();
void NEO_6M_sleep(bool sleep, uint8_t module_id);


#endif
