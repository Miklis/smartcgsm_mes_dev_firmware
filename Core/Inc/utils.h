#ifndef __UTILS_H
#define __UTILS_H
#include <stdint.h>

void utils_increse_circular_buffer_index(uint8_t *index, uint8_t buffer_len);
int8_t utils_hex_num_to_dec(uint8_t hex_num, uint8_t order);

#endif
