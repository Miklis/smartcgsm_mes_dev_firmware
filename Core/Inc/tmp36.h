#ifndef __DATACOLECTOR_TMP_36_H
#define __DATACOLECTOR_TMP_36_H
#include "stm32f1xx_hal.h"
#include "modules.h"

void TMP_36_sleep(bool sleep, uint8_t module_id);
void TMP_36_proccess();
uint8_t TMP_36_get_temperature(Module_return_value_t *data, uint8_t module_id);


#endif
